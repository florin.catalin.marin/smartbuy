package produs;

public class Product {
    private int id;
    private String name;
    private double price;
    private int quantity;
    private String description;
    private boolean checked;
    private int user_id;

    public Product() {
        super();
    }

    public Product(String name, double price, int quantity, String description, boolean checked, int user_id) {
        super();
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
        this.checked = checked;
        this.user_id = user_id;
    }

    public Product(int id, String name, double price, int quantity, String description, boolean checked, int user_id) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
        this.checked = checked;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return " name= " + name + ", price= " + price + ", quantity= " + quantity + ", description= "
                + description + ", checked= " + checked ;
    }


}
