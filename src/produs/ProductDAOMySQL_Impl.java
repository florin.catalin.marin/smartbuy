package produs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import produs.Product;
import produs.ProductDAO;

public class ProductDAOMySQL_Impl implements ProductDAO {
    public static final String CONNECTION_URL = "jdbc:mysql://localhost/lauradb";

    @Override
    public void addProduct(Product prod) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(
                    "insert into Product(Name,Price,Quantity,Description,Checked,User_Id) values(?,?,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, prod.getName());
            ps.setDouble(2, prod.getPrice());
            ps.setInt(3, prod.getQuantity());
            ps.setString(4, prod.getDescription());
            ps.setBoolean(5, prod.isChecked());
            ps.setInt(6, prod.getUser_id());
            int affectedRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                prod.setId(rs.getInt(1));
            }
            closeConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean update(Product prod) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("update Product set Name = ?, Price = ?, Quantity = ?, Description = ?, Checked = ?, User_Id = ? where ProductId = ?");
            ps.setString(1, prod.getName());
            ps.setDouble(2, prod.getPrice());
            ps.setInt(3, prod.getQuantity());
            ps.setString(4, prod.getDescription());
            ps.setBoolean(5, prod.isChecked());
            ps.setInt(6, prod.getUser_id());
            ps.setInt(7, prod.getId());
            int affectedRows = ps.executeUpdate();
            closeConnection(conn);
            return affectedRows == 1;
        } catch (SQLException e) {
            return false;
        }

    }


    @Override
    public boolean delete(int id) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("delete from Product where ProductId = ?");
            ps.setInt(1, id);
            int affectedRows = ps.executeUpdate();
            closeConnection(conn);
            return affectedRows == 1;
        } catch (SQLException e) {
            return false;
        }

    }


    @Override
    public Product findById(int id) {
        Product prod = null;
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from Product where ProductId = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int id2 = rs.getInt("ProductId");
                String name = rs.getString("Name");
                double price = rs.getDouble("Price");
                int quantity = rs.getInt("Quantity");
                String description = rs.getString("Description");
                boolean checked = rs.getBoolean("Checked");
                int user_id = rs.getInt("User_Id");
                prod = new Product(id2, name, price, quantity, description, checked, user_id);
            }
            closeConnection(conn);
            return prod;
        } catch (SQLException e) {
            return null;
        }

    }


    @Override
    public Product[] getAllProducts() {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from Product");
            ResultSet rs = ps.executeQuery();
            List<Product> productList = new ArrayList<Product>();
            while (rs.next()) {
                int id = rs.getInt("ProductId");
                String name = rs.getString("Name");
                double price = rs.getDouble("Price");
                int quantity = rs.getInt("Quantity");
                String description = rs.getString("Description");
                boolean checked = rs.getBoolean("Checked");
                int user_id = rs.getInt("User_Id");
                productList.add(new Product(id, name, price, quantity, description, checked, user_id));
            }
            closeConnection(conn);
            return productList.toArray(new Product[productList.size()]);
        } catch (SQLException e) {
            return null;
        }

    }

    public Product[] getUserProducts(int id) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from Product where User_id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Product> productList = new ArrayList<Product>();
            while (rs.next()) {
                int id2 = rs.getInt("ProductId");
                String name = rs.getString("Name");
                double price = rs.getDouble("Price");
                int quantity = rs.getInt("Quantity");
                String description = rs.getString("Description");
                boolean checked = rs.getBoolean("Checked");
                int user_id = rs.getInt("User_Id");
                productList.add(new Product(id2, name, price, quantity, description, checked, user_id));
            }
            closeConnection(conn);
            return productList.toArray(new Product[productList.size()]);
        } catch (SQLException e) {
            return null;
        }

    }

    /**
     * exception returns this kind of exception SQLException
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL, "root", "1234");

    }

    /**
     * exception returns this kind of exception SQLException
     */
    public void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }
}
