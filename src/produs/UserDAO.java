package produs;

public interface UserDAO {
	void addUser(User u);
	public boolean update(User u);
	public boolean delete(int id);
	public User findById(int id);
	public User[] getAllUsers();
}
