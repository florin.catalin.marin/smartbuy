package produs;

//import java.util.Date;
import java.sql.Date;

public class ShoppingList {
	private int id;
	private String name;
	private Date StartDate;
	private Date EndDate;
	private int user_id;
	
	public ShoppingList() {
		super();
	}

	public ShoppingList(int id, String name, Date startDate, Date endDate, int user_id) {
		super();
		this.id = id;
		this.name = name;
		this.StartDate = startDate;
		this.EndDate = endDate;
		this.user_id= user_id;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return StartDate;
	}

	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}

	public Date getEndDate() {
		return EndDate;
	}

	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	@Override
	public String toString() {
		return " name= " + name + ", Start Date= " + StartDate + ", End Date= " + EndDate;
	}

	
	
	
	
}
