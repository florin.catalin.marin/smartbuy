package produs;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import produs.ShoppingList;
import produs.ShoppingListDAO;

public class ShoppingListDAOMySQL_Impl implements ShoppingListDAO {
    public static final String CONNECTION_URL = "jdbc:mysql://localhost/lauradb";

    public void addShoppingListProduct(ShoppingList shoppingList, Product product) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(
                    "insert into AddProduct(Product_Id,ShoppList_Id) values(?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, product.getId());
            ps.setInt(2, shoppingList.getId());
            int affectedRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
//            if (rs.next()) {
//                shopp.setId(rs.getInt(1));
//            }
            closeConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Product[] getProductForList(ShoppingList shoppingList) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from Product " +
                            "INNER JOIN AddProduct ON Product.ProductId = AddProduct.Product_Id " +
                            "INNER JOIN ShoppingList ON AddProduct.ShoppList_Id = ShoppingList.ShoppingListId " +
                            "WHERE ShoppingList.ShoppingListId = ?");
            ps.setInt(1, shoppingList.getId());
            ResultSet rs = ps.executeQuery();
            List<Product> productList = new ArrayList<Product>();
            while (rs.next()) {
                int id2 = rs.getInt("ProductId");
                String name = rs.getString("Name");
                double price = rs.getDouble("Price");
                int quantity = rs.getInt("Quantity");
                String description = rs.getString("Description");
                boolean checked = rs.getBoolean("Checked");
                int user_id = rs.getInt("User_Id");
                productList.add(new Product(id2, name, price, quantity, description, checked, user_id));
            }
            closeConnection(conn);
            return productList.toArray(new Product[productList.size()]);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public void addShoppingList(ShoppingList shopp) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(
                    "insert into ShoppingList(Name,StartDate,EndDate,User_Id) values(?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, shopp.getName());
            ps.setDate(2, shopp.getStartDate());
            ps.setDate(3, shopp.getEndDate());
            ps.setInt(4, shopp.getUser_id());

            int affectedRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                shopp.setId(rs.getInt(1));
            }
            closeConnection(conn);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    public boolean update(ShoppingList shopp) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("update ShoppingList set Name = ?, StartDate = ?, EndDate = ?, User_Id = ? where ShoppingListId = ?");
            ps.setString(1, shopp.getName());
            ps.setDate(2, shopp.getStartDate());
            ps.setDate(3, shopp.getEndDate());
            ps.setInt(4, shopp.getUser_id());
            ps.setInt(5, shopp.getId());
            int affectedRows = ps.executeUpdate();
            closeConnection(conn);
            return affectedRows == 1;
        } catch (SQLException e) {
            return false;
        }

    }


    @Override
    public boolean delete(int id) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("delete from ShoppingList where ShoppingListId = ?");
            ps.setInt(1, id);
            int affectedRows = ps.executeUpdate();
            closeConnection(conn);
            return affectedRows == 1;
        } catch (SQLException e) {
            return false;
        }

    }


    @Override
    public ShoppingList findById(int id) {
        ShoppingList shopp = null;
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from ShoppingList where ShoppingListId = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int id2 = rs.getInt("ShoppingListId");
                String name = rs.getString("Name");
                Date startdate = rs.getDate("StartDate");
                Date enddate = rs.getDate("EndDate");
                int user_id = rs.getInt("User_Id");
                shopp = new ShoppingList(id2, name, startdate, enddate, user_id);
            }
            closeConnection(conn);
            return shopp;
        } catch (SQLException e) {
            return null;
        }

    }


    @Override
    public ShoppingList[] getAllShoppingLists() {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from ShoppingList");
            ResultSet rs = ps.executeQuery();
            List<ShoppingList> shopplistList = new ArrayList<ShoppingList>();
            while (rs.next()) {
                int id = rs.getInt("ShoppingListId");
                String name = rs.getString("Name");
                Date startdate = rs.getDate("StartDate");
                Date enddate = rs.getDate("EndDate");
                int user_id = rs.getInt("User_Id");
                shopplistList.add(new ShoppingList(id, name, startdate, enddate, user_id));
            }
            closeConnection(conn);
            return shopplistList.toArray(new ShoppingList[shopplistList.size()]);
        } catch (SQLException e) {
            return null;
        }

    }

    @Override

    public ShoppingList[] getUserShoppingLists(int id) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from ShoppingList where User_id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<ShoppingList> shopplistList = new ArrayList<>();
            while (rs.next()) {
                int id2 = rs.getInt("ShoppingListId");
                String name = rs.getString("Name");
                Date startdate = rs.getDate("StartDate");
                Date enddate = rs.getDate("EndDate");
                int user_id = rs.getInt("User_Id");
                shopplistList.add(new ShoppingList(id2, name, startdate, enddate, user_id));
            }
            closeConnection(conn);
            return shopplistList.toArray(new ShoppingList[shopplistList.size()]);
        } catch (SQLException e) {
            return null;
        }

    }

    /**
     * exception returns this kind of exception SQLException
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL, "root", "1234");

    }

    /**
     * exception returns this kind of exception SQLException
     */
    public void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }
}
