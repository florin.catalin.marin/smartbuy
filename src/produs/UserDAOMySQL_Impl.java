package produs;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAOMySQL_Impl implements UserDAO {

    public static final String CONNECTION_URL = "jdbc:mysql://localhost/lauradb";

    @Override
    public void addUser(User user) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement(
                    "insert into User(Name,Password) values(?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getName());
            ps.setString(2, user.getPassword());
            int affectedRows = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
            }
            closeConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean update(User user) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("update User set Name = ?, Password = ? where UserId = ?");
            ps.setString(1, user.getName());
            ps.setString(2, user.getPassword());
            ps.setInt(3, user.getId());
            int affectedRows = ps.executeUpdate();
            closeConnection(conn);
            return affectedRows == 1;
        } catch (SQLException e) {
            return false;
        }

    }


    @Override
    public boolean delete(int id) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("delete from User where UserId = ?");
            ps.setInt(1, id);
            int affectedRows = ps.executeUpdate();
            closeConnection(conn);
            return affectedRows == 1;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }

    }


    @Override
    public User findById(int id) {
        User user = null;
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from User where UserId = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int id2 = rs.getInt("UserId");
                String name = rs.getString("Name");
                String password = rs.getString("Password");
                user = new User(id2, name, password);
            }
            closeConnection(conn);
            return user;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }

    }


    @Override
    public User[] getAllUsers() {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn
                    .prepareStatement("select * from User");
            ResultSet rs = ps.executeQuery();
            List<User> userList = new ArrayList<User>();
            while (rs.next()) {
                int id = rs.getInt("UserId");
                String name = rs.getString("Name");
                String password = rs.getString("Password");
                userList.add(new User(id, name, password));
            }
            closeConnection(conn);
            return userList.toArray(new User[userList.size()]);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    /**
     * exception returns this kind of exception SQLException
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL, "root", "1234");

    }

    /**
     * exception returns this kind of exception SQLException
     */
    public void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }
}
