package produs;


import produs.ShoppingListDAO;
import produs.ShoppingListDAOMySQL_Impl;


public class ShoppingListDAOFactory {

	ShoppingListDAO createShoppingListDAO()
	{
		return new ShoppingListDAOMySQL_Impl();
		
	}
}
