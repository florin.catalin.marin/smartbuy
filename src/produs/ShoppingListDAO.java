package produs;

public interface ShoppingListDAO {

    void addShoppingList(ShoppingList sl);

    void addShoppingListProduct(ShoppingList shoppingList, Product product);

    public boolean update(ShoppingList sl);

    public boolean delete(int id);

    public ShoppingList findById(int id);

    public ShoppingList[] getAllShoppingLists();

    public ShoppingList[] getUserShoppingLists(int id);

    public Product[] getProductForList(ShoppingList shoppingList);
}
