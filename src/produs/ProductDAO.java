package produs;
import java.util.List;

public interface ProductDAO {
	void addProduct(Product p);
	public boolean update(Product p);
	public boolean delete(int id);
	public Product findById(int id);
	public Product[] getAllProducts();
	public Product[] getUserProducts(int id);
}


