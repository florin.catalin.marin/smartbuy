package produs;

import produs.exception.ExitException;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String __MENU__ = "  Menu";
    private static final String ALL_PRODUCTS = "See all products";
    private static final String ALL_LISTS = "See all lists";
    private static final String NEW_PRODUCT = "Add a new product";
    private static final String EDIT_PRODUCT = "Edit a product";
    private static final String NEW_LIST = "Add a new list";
    private static final String EDIT_LIST = "Edit a list";
    private static final String SEPARATOR = "---------";
    private static final String EXIT = "0. Exit";
    private static final String EMPTY = "";
    private static final String BACK = "Back to menu";
    private static final String ADD_PRODUCT_LIST = "Add a product to a list";
    private static final String SEE_LIST = "See a list";
    private static final String LOG_IN = "Log in";
    private static final String NEW_USER = "New user";
    private static final String NEW_PRODUCT_NAME = "Enter product name:";
    private static final String NEW_PRODUCT_PRICE = "Enter product price: ";
    private static final String NEW_PRODUCT_QUANTITY = "Enter product quantity: ";
    private static final String NEW_PRODUCT_DESCRIPTION = "Enter product description: ";
    private static final String NEW_PRODUCT_CHECKED = "Enter product checked: ";
    private static final String CHOOSE_PRODUCT = "Enter product id from the list: ";
    private static final String CHECK_PRODUCT = "Check product: ";
    private static final String CHOOSE_LIST = "Enter shopping list id from the list: ";


    private static UserDAO userDAO;
    private static UserDAOFactory userDAOFactory;

    private static ProductDAOFactory productDAOFactory;
    private static ProductDAO productDAO;

    private static ShoppingListDAOFactory shoppingListDAOFactory;
    private static ShoppingListDAO shoppingListDAO;


    private static void init() {
        userDAOFactory = new UserDAOFactory();
        userDAO = userDAOFactory.createUserDAO();

        productDAOFactory = new ProductDAOFactory();
        productDAO = productDAOFactory.createProductDAO();

        shoppingListDAOFactory = new ShoppingListDAOFactory();
        shoppingListDAO = shoppingListDAOFactory.createShoppingListDAO();
    }

    private static void showMessage(String message, int index) {
        if (index == 0) {
            System.out.println(message);
        } else {
            System.out.println(index + ". " + message);
        }
    }

    private static void showMessages(String[] messages, int start) {
        for (String message : messages) {
            showMessage(message, ++start);
        }
    }

    private static void showMessages(List<String> messages, int start) {
        for (String message : messages) {
            showMessage(message, ++start);
        }
    }

    private static void showMenu() {
        showMessage(__MENU__, 0);
        showMessage(SEPARATOR, 0);
        showMessages(new String[]{ALL_PRODUCTS, ALL_LISTS, NEW_PRODUCT, NEW_LIST, EDIT_PRODUCT, EDIT_LIST}, 0);
        showMessage(EXIT, 0);
    }

    private static void displayMenu(String[] messages) {
        showMessage(EMPTY, 0);
        showMessages(messages, 0);
        showMessage(EXIT, 0);
    }

    private static void showMenu3() {


//		Scanner keyboard2 = new Scanner(System.in);
//		int next2 = keyboard2.nextInt();
//		if (next2 == 2) {
//			System.out.println("2 ales");
//		}
    }

    private static Login getLogin() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Name?");
        String name = keyboard.next();
        System.out.println("Password?");
        String password = keyboard.next();

        return new Login(name, password);
    }

    private static String confirmPassword() {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Confirm Password?");
        return keyboard.next();
    }

    private static User signUp() {
        Login login = getLogin();
        if (login.checkPassword(confirmPassword())) {
            System.out.println("User added!");

            User user = new User(1, login.getName(), login.getPassword());
            user.setPassword(login.getPassword()); // @toDO fix hack for hashing
            userDAO.addUser(user);
            return user;
        }

        System.out.println("Should be the same password!");
        return null;
    }

    private static User login() {
        Login login = getLogin();

        for (User user : userDAO.getAllUsers()) {
            if (!user.getName().equals(login.getName()) || !user.checkPassword(login.getPassword())) {
                continue;
            }

            return user;
        }
        System.out.println("Invalid Name or Password");

        return null;
    }

    private static User authenticate() {
        Scanner keyboard = new Scanner(System.in);

        showMessage(__MENU__, 0);
        showMessage(SEPARATOR, 0);
        showMessages(new String[]{LOG_IN, NEW_USER}, 0);

        switch (Integer.parseInt(keyboard.nextLine())) {
            case 1:
                return login();
            case 2:
                return signUp();
        }

        return null;

    }

    private static void test() {

//        Product p1 = new Product(1, "ulei", 6.3, 2, "ulei Bunica", false, 2);
//        Product p2 = new Product(2, "otet", 3.5, 1, "otet alb", false, 2);
//        productDAO.addProduct(p1);
//        productDAO.addProduct(p2);
//        productDAO.delete(4);
//
//        Product foundCart = productDAO.findById(6);
//        foundCart.setName("ulei");
//        foundCart.setPrice(3.5);
//        productDAO.update(foundCart);
//
//        Product[] list = productDAO.getAllProducts();
//        for (int i = 0; i < list.length; i++)
//            System.out.println(list[i].toString());
//
//        ShoppingList sl1 = new ShoppingList(1, "zi de nastere", new Date(2018 - 1901, 12, 25), new Date(2018 - 1901, 12, 30), 1);
//        shoppingListDAO.addShoppingList(sl1);

//        userDAO.addUser(new User(1, "Maria", "121212"));
    }


    private static List<String> displayProducts(Product[] products, User user) {         // @todo  nu cred ca e bine
        List<String> productsMessage = new ArrayList<>();
        for (Product product : products) {
            if (user.getId() != product.getUser_id()) {
                continue;
            }
            productsMessage.add(product.toString());
        }

        return productsMessage;
    }

    private static List<String> displayLists(ShoppingList[] shoppingLists, User user) {
        List<String> shoppingListsMessage = new ArrayList<>();
        for (ShoppingList shoppingList : shoppingLists) {
            if (user.getId() != shoppingList.getUser_id()) {
                continue;
            }

            shoppingListsMessage.add(shoppingList.toString());
        }

        return shoppingListsMessage;
    }

    private static Product[] showProductList(User user) {
        ShoppingList shoppingList = chooseShoppingList(user);
        Product[] products = shoppingListDAO.getProductForList(shoppingList);
        showMessages(displayProducts(products, user), 0);

        return products;
    }

    private static ShoppingList chooseShoppingList(User user) {
        Scanner keyboard = new Scanner(System.in);

        showMessage(CHOOSE_LIST, 0);
        ShoppingList[] shoppingLists = shoppingListDAO.getUserShoppingLists(user.getId());
        showMessages(displayLists(shoppingLists, user), 0);


        Integer shoppingListIndex = Integer.parseInt(keyboard.nextLine());
        Integer counter = 1;
        ShoppingList shoppingList = null;

        for (ShoppingList shoppingListParser : shoppingLists) {
            if (shoppingListIndex == counter) {
                shoppingList = shoppingListParser;
                break;
            }
            counter++;
        }

        return shoppingList;
    }

    private static Product matchProduct(Product[] products, User user) {
        Scanner keyboard = new Scanner(System.in);
        showMessage(CHOOSE_PRODUCT, 0);
        showMessages(displayProducts(products, user), 0);

        Integer productIndex = Integer.parseInt(keyboard.nextLine());
        Integer counter = 1;
        Product product = null;
        for (Product productsParser : products) {
            if (productIndex == counter) {
                product = productsParser;
                break;
            }
            counter++;
        }

        return product;
    }

    private static Product chooseProduct(User user) {
        Product[] products = productDAO.getUserProducts(user.getId());
        return matchProduct(products, user);
    }

    private static Product chooseProduct(Product[] products, User user) {
        return matchProduct(products, user);
    }


    private static void addProductList(Product product, User user) {
        Scanner keyboard = new Scanner(System.in);
        ShoppingList shoppingList;
        boolean y;

//        do {
//            product = chooseProduct(user);
//            showMessage("Continue with product: " + product.toString() + " ? ( y/n)", 0);
//            y = keyboard.nextLine().equals("y");
//        } while (!y);

        do {
            shoppingList = chooseShoppingList(user);
            showMessage("Continue with Shopping list: " + shoppingList.toString() + " ? ( y/n)", 0);
            y = keyboard.nextLine().equals("y");
        } while (!y);

        shoppingListDAO.addShoppingListProduct(shoppingList, product);
        showMessage("Product: " + product.toString() + " Added to list: " + shoppingList.toString(), 0);
    }

    private static Product addProduct(User user) {
        Scanner keyboard = new Scanner(System.in);
        Product product = new Product();
        product.setUser_id(user.getId());

        showMessage(NEW_PRODUCT_NAME, 0);
        product.setName(keyboard.nextLine());
        showMessage(NEW_PRODUCT_PRICE, 0);
        product.setPrice(Double.parseDouble(keyboard.nextLine()));
        showMessage(NEW_PRODUCT_QUANTITY, 0);
        product.setQuantity(Integer.parseInt(keyboard.nextLine()));
        showMessage(NEW_PRODUCT_DESCRIPTION, 0);
        product.setDescription(keyboard.nextLine());
        // showMessage(NEW_PRODUCT_CHECKED, 0);
        // product.setChecked(Boolean.parseBoolean(keyboard.nextLine()));
        product.setChecked(false);
        showMessage("Add product: " + product.toString() + " ? ( y/n)", 0);

        if (keyboard.next().equals("y")) {
            productDAO.addProduct(product);
            System.out.println("Product added");
        }
        return product;
    }


    private static Product editProduct(Product product) {
        Scanner keyboard = new Scanner(System.in);

        showMessage(NEW_PRODUCT_NAME + " / Current product name: " + product.getName(), 0);
        String name = keyboard.nextLine();
        product.setName("".equals(name) ? product.getName() : name);
        showMessage(NEW_PRODUCT_PRICE + " / Current product price: " + product.getPrice(), 0);
        double price = 0D;
        try {
            price = Double.parseDouble(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }
        product.setPrice(0 == price ? product.getPrice() : price);
        showMessage(NEW_PRODUCT_QUANTITY + " / Current product quantity: " + product.getQuantity(), 0);
        int quantity = 0;
        try {
            quantity = Integer.parseInt(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }
        product.setQuantity(0 == quantity ? product.getQuantity() : quantity);
        showMessage(NEW_PRODUCT_DESCRIPTION + " / Current product description: " + product.getDescription(), 0);
        String description = keyboard.nextLine();
        product.setDescription("".equals(description) ? product.getDescription() : description);
        product.setChecked(false);
        showMessage("Edit product: " + product.toString() + " ? ( y/n)", 0);

        if (keyboard.next().equals("y")) {
            productDAO.update(product);
            System.out.println("Product updated");
        }
        return product;
    }

    private static Product checkProduct(Product product) {
        Scanner keyboard = new Scanner(System.in);

        showMessage("Current product name: " + product.getName(), 0);
        showMessage("Current product price: " + product.getPrice(), 0);
        showMessage("Current product quantity: " + product.getQuantity(), 0);
        showMessage("Current product description: " + product.getDescription(), 0);
        showMessage("Check product: true or false / Current product checked: " + product.isChecked(), 0);
        try {
            product.setChecked(Boolean.parseBoolean(keyboard.nextLine()));
        } catch (Exception ignored) {
        }
        showMessage("Check product: " + product.toString() + " ? ( y/n)", 0);

        if (keyboard.next().equals("y")) {
            productDAO.update(product);
            System.out.println("Product updated");
        }
        return product;
    }

    private static ShoppingList editShoppingList(ShoppingList shoppingList) {
        Scanner keyboard = new Scanner(System.in);

        showMessage("List name: / Current list name: " + shoppingList.getName(), 0);
        String name = keyboard.nextLine();
        shoppingList.setName("".equals(name) ? shoppingList.getName() : name);

        showMessage("List start date: year: / Current list start date: year: " + shoppingList.getStartDate(), 0);
        int listStartYear = 0;
        try {
            listStartYear = Integer.parseInt(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }

        showMessage("List start date: month: / Current list start date: month: " + shoppingList.getStartDate(), 0);
        int listStartMonth = 0;
        try {
            listStartMonth = Integer.parseInt(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }

        showMessage("List start date: day: / Current list start date: day: " + shoppingList.getStartDate(), 0);
        int listStartDay = 0;
        try {
            listStartDay = Integer.parseInt(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }
        if (listStartDay != 0 && listStartMonth != 0 && listStartYear != 0) {
            shoppingList.setStartDate(new Date(listStartYear - 1900, listStartMonth - 1, listStartDay));
        }

        showMessage("List end date: year: / Current list end date: year: " + shoppingList.getEndDate(), 0);
        int listEndYear = 0;
        try {
            listEndYear = Integer.parseInt(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }

        showMessage("List end date: month: / Current list end date: month: " + shoppingList.getEndDate(), 0);
        ;
        int listEndMonth = 0;
        try {
            listEndMonth = Integer.parseInt(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }

        showMessage("List end date: day: / Current list end date: day: " + shoppingList.getEndDate(), 0);
        int listEndDay = 0;
        try {
            listEndDay = Integer.parseInt(keyboard.nextLine());
        } catch (NumberFormatException ignored) {
        }
        if (listEndDay != 0 && listEndMonth != 0 && listEndYear != 0) {
            shoppingList.setEndDate(new Date(listEndYear - 1900, listEndMonth - 1, listEndDay));
        }
        showMessage("Edit shopping list: " + shoppingList.toString() + " ? ( y/n)", 0);

        if (keyboard.next().equals("y")) {
            shoppingListDAO.update(shoppingList);
            System.out.println("Shopping list updated");
        }

        return shoppingList;
    }

    private static void addShoppingList(User user) {
        Scanner keyboard = new Scanner(System.in);
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.setUser_id(user.getId());
        Boolean ok = true;

        do {
            System.out.println("List name is:");
            shoppingList.setName(keyboard.nextLine());

            System.out.print("List start date: year:");
            int listStartYear = Integer.parseInt(keyboard.nextLine());
            System.out.print("List start date: month:");
            int listStartMonth = Integer.parseInt(keyboard.nextLine());
            System.out.print("List start date: day:");
            int listStartDay = Integer.parseInt(keyboard.nextLine());
            shoppingList.setStartDate(new Date(listStartYear - 1900, listStartMonth - 1, listStartDay));

            System.out.print("List end date: year:");
            int listEndYear = Integer.parseInt(keyboard.nextLine());
            System.out.print("List end date: month:");
            int listEndMonth = Integer.parseInt(keyboard.nextLine());
            System.out.print("List end date: day:");
            int listEndDay = Integer.parseInt(keyboard.nextLine());
            shoppingList.setEndDate(new Date(listEndYear - 1900, listEndMonth - 1, listEndDay));

            if (new Date(listStartYear - 1900, listStartMonth - 1, listStartDay).after(new Date(listEndYear - 1900, listEndMonth - 1, listEndDay))) {
                System.out.println("End Date should be after Start Date");
            } else
                ok = false;

        } while (ok);
        System.out.println("Add list:" + shoppingList.toString() + " ? ( y/n)");

        if (keyboard.next().equals("y")) {
            shoppingListDAO.addShoppingList(shoppingList);
            System.out.println("List added");
        }
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        init();
//        test();

        try {
            User user = null;
            while (null == user) {
                user = authenticate();
            }

            while (true) {
                showMenu();
                int nextInt;
                try {
                    nextInt = Integer.parseInt(keyboard.nextLine());
                } catch (NumberFormatException ignored) {
                    continue;
                }

                switch (nextInt) {
                    case 1:
                        Product[] products = productDAO.getUserProducts(user.getId());
                        showMessages(displayProducts(products, user), 0);
                        displayMenu(new String[]{BACK, ADD_PRODUCT_LIST});
                        switch (Integer.parseInt(keyboard.nextLine())) {
                            case 2:
                                Product product = chooseProduct(user);
                                editProduct(product);
                                addProductList(product, user);
                                break;
                            case 0:
                                throw new ExitException();
                        }
                        break;
                    case 2:
                        ShoppingList[] shoppingLists = shoppingListDAO.getUserShoppingLists(user.getId());
                        showMessages(displayLists(shoppingLists, user), 0);
                        displayMenu(new String[]{BACK, SEE_LIST});
                        switch (Integer.parseInt(keyboard.nextLine())) {
                            case 2:
                                Product[] listProducts = showProductList(user);
                                displayMenu(new String[]{BACK, CHECK_PRODUCT});
                                switch (Integer.parseInt(keyboard.nextLine())) {
                                    case 2:
                                        Product product = chooseProduct(listProducts, user);
                                        checkProduct(product);
                                        break;
                                    case 0:
                                        throw new ExitException();
                                }
                                break;
                            case 0:
                                throw new ExitException();
                        }
                        break;
                    case 3:
                        addProduct(user);
                        break;
                    case 4:
                        addShoppingList(user);
                        break;
                    case 5:
                        Product product = chooseProduct(user);
                        editProduct(product);
                        break;
                    case 6:
                        ShoppingList shoppingList = chooseShoppingList(user);
                        editShoppingList(shoppingList);
                        break;
                    case 0:
                        throw new ExitException();
                }
            }

        } catch (ExitException exception) {
            System.out.println("Exit system");
            System.exit(0);
        }
    }


}
